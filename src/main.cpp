#ifdef ESP8266
  #include <ESP8266WiFi.h>
#else
  #include <WiFi.h>
#endif
#include <PubSubClient.h>
#include <WiFiClientSecure.h>

static const char *root_ca PROGMEM = R"EOF(
-----BEGIN CERTIFICATE-----
MIIFazCCA1OgAwIBAgIRAIIQz7DSQONZRGPgu2OCiwAwDQYJKoZIhvcNAQELBQAw
TzELMAkGA1UEBhMCVVMxKTAnBgNVBAoTIEludGVybmV0IFNlY3VyaXR5IFJlc2Vh
cmNoIEdyb3VwMRUwEwYDVQQDEwxJU1JHIFJvb3QgWDEwHhcNMTUwNjA0MTEwNDM4
WhcNMzUwNjA0MTEwNDM4WjBPMQswCQYDVQQGEwJVUzEpMCcGA1UEChMgSW50ZXJu
ZXQgU2VjdXJpdHkgUmVzZWFyY2ggR3JvdXAxFTATBgNVBAMTDElTUkcgUm9vdCBY
MTCCAiIwDQYJKoZIhvcNAQEBBQADggIPADCCAgoCggIBAK3oJHP0FDfzm54rVygc
h77ct984kIxuPOZXoHj3dcKi/vVqbvYATyjb3miGbESTtrFj/RQSa78f0uoxmyF+
0TM8ukj13Xnfs7j/EvEhmkvBioZxaUpmZmyPfjxwv60pIgbz5MDmgK7iS4+3mX6U
A5/TR5d8mUgjU+g4rk8Kb4Mu0UlXjIB0ttov0DiNewNwIRt18jA8+o+u3dpjq+sW
T8KOEUt+zwvo/7V3LvSye0rgTBIlDHCNAymg4VMk7BPZ7hm/ELNKjD+Jo2FR3qyH
B5T0Y3HsLuJvW5iB4YlcNHlsdu87kGJ55tukmi8mxdAQ4Q7e2RCOFvu396j3x+UC
B5iPNgiV5+I3lg02dZ77DnKxHZu8A/lJBdiB3QW0KtZB6awBdpUKD9jf1b0SHzUv
KBds0pjBqAlkd25HN7rOrFleaJ1/ctaJxQZBKT5ZPt0m9STJEadao0xAH0ahmbWn
OlFuhjuefXKnEgV4We0+UXgVCwOPjdAvBbI+e0ocS3MFEvzG6uBQE3xDk3SzynTn
jh8BCNAw1FtxNrQHusEwMFxIt4I7mKZ9YIqioymCzLq9gwQbooMDQaHWBfEbwrbw
qHyGO0aoSCqI3Haadr8faqU9GY/rOPNk3sgrDQoo//fb4hVC1CLQJ13hef4Y53CI
rU7m2Ys6xt0nUW7/vGT1M0NPAgMBAAGjQjBAMA4GA1UdDwEB/wQEAwIBBjAPBgNV
HRMBAf8EBTADAQH/MB0GA1UdDgQWBBR5tFnme7bl5AFzgAiIyBpY9umbbjANBgkq
hkiG9w0BAQsFAAOCAgEAVR9YqbyyqFDQDLHYGmkgJykIrGF1XIpu+ILlaS/V9lZL
ubhzEFnTIZd+50xx+7LSYK05qAvqFyFWhfFQDlnrzuBZ6brJFe+GnY+EgPbk6ZGQ
3BebYhtF8GaV0nxvwuo77x/Py9auJ/GpsMiu/X1+mvoiBOv/2X/qkSsisRcOj/KK
NFtY2PwByVS5uCbMiogziUwthDyC3+6WVwW6LLv3xLfHTjuCvjHIInNzktHCgKQ5
ORAzI4JMPJ+GslWYHb4phowim57iaztXOoJwTdwJx4nLCgdNbOhdjsnvzqvHu7Ur
TkXWStAmzOVyyghqpZXjFaH3pO3JLF+l+/+sKAIuvtd7u+Nxe5AW0wdeRlN8NwdC
jNPElpzVmbUq4JUagEiuTDkHzsxHpFKVK7q4+63SM1N95R1NbdWhscdCb+ZAJzVc
oyi3B43njTOQ5yOf+1CceWxG1bQVs5ZufpsMljq4Ui0/1lvh+wjChP4kqKOJ2qxq
4RgqsahDYVvTH9w7jXbyLeiNdd8XM2w9U/t7y0Ff/9yi0GE44Za4rF2LN9d11TPA
mRGunUHBcnWEvgJBQl9nJEiU0Zsnvgc/ubhPgXRR4Xq37Z0j4r7g1SgEEzwxA57d
emyPxgcYxn/eR44/KJ4EBs+lVDR3veyJm+kXQ99b21/+jh5Xos1AnX5iItreGCc=
-----END CERTIFICATE-----
)EOF";

#define IrLaneA 15
#define IrLaneB 4 
#define IrLaneAConf 5
#define IrLaneBConf 19
#define RelayLaneA 33
#define RelayLaneB 14
#define TimeoutInMillis 60*1000 //60 secs
#define MotionTimeoutInMillis 2*1000 // 2 secs

const char *ssid = ""; // Your WiFi SSID
const char *password = ""; // Your WiFi Password
const char *mqtt_server = ""; // MQTT Server Address
const char *mqtt_username = ""; // MQTT Username
const char *mqtt_password = ""; // MQTT Password
const int mqtt_port = 8883;
unsigned long LaneATimer = 0;
unsigned long LaneBTimer = 0;
unsigned long LaneAMotionDetectedTimer = 0;
unsigned long LaneBMotionDetectedTimer = 0;
unsigned long now = 0;
bool isOverRidden = false;
String clientId;

WiFiClientSecure espClient;
/**** MQTT Client Initialisation Using WiFi Connection *****/
PubSubClient client(espClient);

void handleLane(unsigned int relay, unsigned int ir, unsigned int irconf, unsigned long* timer, unsigned long* motionTimer, String laneName);
boolean publishMessage(const char *topic, String payload);
void callback(char *topic, byte *payload, unsigned int length);
void handleOverride(const String& incomingMessage);
void handleLaneOverride(unsigned int relay, unsigned long* timer, String laneName, const String& incomingMessage);
void setup_wifi();
void reconnect();

void setup() {
  pinMode(IrLaneA, INPUT);
  pinMode(IrLaneB, INPUT);
  pinMode(IrLaneAConf, INPUT);
  pinMode(IrLaneBConf, INPUT);
  pinMode(RelayLaneA, OUTPUT);
  pinMode(RelayLaneB, OUTPUT);
  digitalWrite(RelayLaneA, HIGH);
  digitalWrite(RelayLaneB, HIGH);
  setup_wifi();

  #ifdef ESP8266
    espClient.setInsecure();
  #else
    espClient.setCACert(root_ca);
  #endif
  client.setServer(mqtt_server, mqtt_port);
  client.setCallback(callback);
}

void loop() {
  now = millis();
  if (!client.connected()) reconnect();  // check if client is connected
  client.loop();
  
  if(LaneAMotionDetectedTimer != 0 && (now - LaneAMotionDetectedTimer > MotionTimeoutInMillis)) {
    LaneAMotionDetectedTimer = 0;
    handleLane(RelayLaneA, IrLaneA, IrLaneAConf, &LaneATimer, &LaneAMotionDetectedTimer, "LaneA");
  }else {
    handleLane(RelayLaneA, IrLaneA, IrLaneAConf, &LaneATimer, &LaneAMotionDetectedTimer, "LaneA"); 
  }

  if(LaneBMotionDetectedTimer != 0 && (now - LaneBMotionDetectedTimer > MotionTimeoutInMillis)) {
    LaneBMotionDetectedTimer = 0;
    handleLane(RelayLaneB, IrLaneB, IrLaneBConf, &LaneBTimer, &LaneBMotionDetectedTimer, "LaneB");
  }else {
    handleLane(RelayLaneB, IrLaneB, IrLaneBConf, &LaneBTimer, &LaneBMotionDetectedTimer, "LaneB");
  }
}

void handleLane(unsigned int relay, unsigned int ir, unsigned int irconf, unsigned long* timer, unsigned long* motionTimer, String laneName){
    boolean timedout = (now - *timer) >= TimeoutInMillis;
    if(*timer != 0 && timedout){
       if(!isOverRidden) digitalWrite(relay, HIGH);
       *timer = 0;
        String message = laneName+"ResetCount";
        publishMessage(clientId.c_str(), message.c_str());
    }
    int motion = digitalRead(ir);
    int confMotion = digitalRead(irconf);
    if(motion == LOW && *motionTimer == 0) {
      *motionTimer = now;
    }
    else if(confMotion == LOW && *motionTimer != 0){
      if(!isOverRidden) digitalWrite(relay, LOW);
      String message = laneName+"Count";
      publishMessage(clientId.c_str(), message.c_str());
      *timer = millis();
      *motionTimer = 0;
    }else if(motion == LOW && confMotion == LOW){
      return;
    }
}

boolean publishMessage(const char *topic, String payload) {
  return client.publish(topic, payload.c_str());
}

void callback(char *topic, byte *payload, unsigned int length) {
  String incommingMessage = "";
  for(int i = 0; i < length ; i++) incommingMessage += (char)payload[i];
  if(strcmp(topic, clientId.c_str()) == 0) handleOverride(incommingMessage);
  if(strcmp(topic, "override_all_nodes") == 0) handleOverride(incommingMessage);
}

void handleOverride(const String& incomingMessage) {
    const char* topic = clientId.c_str();
    if (incomingMessage.equals("override")) isOverRidden = !isOverRidden;
    if (incomingMessage.equals("laneAReset")) {
      digitalWrite(RelayLaneA, HIGH);
      LaneATimer = 0;
    }

    if (incomingMessage.equals("laneBReset")) {
      digitalWrite(RelayLaneB, HIGH);
      LaneBTimer = 0;
    }

    if (incomingMessage.equals("pingSection")) {
        digitalWrite(RelayLaneA, LOW);
        delay(1000);
        digitalWrite(RelayLaneA, HIGH);
    }
    if(!isOverRidden) return;
    handleLaneOverride(RelayLaneA, &LaneATimer, "laneA", incomingMessage);
    handleLaneOverride(RelayLaneB, &LaneBTimer, "laneB", incomingMessage);
}

void handleLaneOverride(unsigned int relay, unsigned long* timer, String laneName, const String& incomingMessage){
  if (incomingMessage.equals(laneName + "On")) digitalWrite(relay, LOW);
  else if (incomingMessage.equals(laneName + "Off")) digitalWrite(relay, HIGH);
}

void setup_wifi() {
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) delay(500);
  randomSeed(micros());
}

void reconnect() {
  if(client.connected()) return;
  do{
    clientId = "client-";// Create a random client ID
    clientId += String(random(0xffff), HEX);
    !client.connect(clientId.c_str(), mqtt_username, mqtt_password);
    delay(5000);
  }while (!client.connected());
  delay(5000);
  publishMessage("connections", clientId);
  client.subscribe(clientId.c_str());
  client.subscribe("override_all");
}

